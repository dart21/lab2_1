import 'package:lab_2/lab_2.dart' as lab_2;
import 'package:test/test.dart';
import 'dart:io';

bool isPrime(N) {
  for (var i = 2; i <= N / i; ++i) {
    if (N % i == 0) {
      return false;
    }
  }
  return true;
}


void main() {
  print(' Enter N: ');
  var N = int.parse(stdin.readLineSync()!);
  if (isPrime(N)) {
    print('$N is a prime number.');
  } else {
    print('$N is not a prime number.');
  }
}
